import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default function ContextScreen() {
    return (
        <View style={styles.container}>
            <Text>ContextScreen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#9eaef3',
      flex: 1,
      alignItems: 'center',
      padding: 10,
    },
  

  });
  
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export default function Home({navigation}: any) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.buttonNav}
        onPress={() => navigation.navigate('Calculator')}>
        <Text style={styles.textButton}>Calculator</Text>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.buttonNav}
        onPress={() => navigation.navigate('ContextApp')}>
        <Text style={styles.textButton}>Context</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#9eaef3',
    flex: 1,
    alignItems: 'center',
    padding: 10,
  },

  buttonNav: {
    backgroundColor: '#490404',
    color: 'white',
    width: '50%',
    padding: 10,
    marginTop:10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  textButton: {
    color: 'white',
    fontSize: 18,
  },
});

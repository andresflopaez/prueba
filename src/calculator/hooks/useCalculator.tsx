import React, { useRef, useState } from 'react'
import { View, Text } from 'react-native'
enum Operators {
    addition,
    subtraction,
    multiplication,
    division,
  }
export default function useCalculator() {
    const [BeforeNumber, setBeforeNumber] = useState<string>('0');
  const [number, setNumber] = useState<string>('0');
  const calcRef = useRef<Operators>();
  

  const clearResult = () => {
    setNumber('0');
    setBeforeNumber('0');
  };

  const makeNumber = (numberTxt: string) => {
    if (
      numberTxt == '+/-' ||
      numberTxt == '/' ||
      numberTxt == 'del' ||
      numberTxt == '÷' ||
      numberTxt == 'x' ||
      numberTxt == '+' ||
      numberTxt == '-' ||
      numberTxt == '='
    ) {
      operationsEspecials(numberTxt);
    } else {
      // no accept double point numbers
      if (number.includes('.') && numberTxt === '.') return;

      if (number.startsWith('0') || number.startsWith('-0')) {
        //decimal point
        if (numberTxt === '.') {
          setNumber(number + numberTxt);

          // number is second zero
        } else if (numberTxt === '0' && number.includes('.')) {
          setNumber(number + numberTxt);
        } else if (numberTxt !== '0' && !number.includes('.')) {
          setNumber(numberTxt);
        } else if (numberTxt === '0' && !number.includes('.')) {
          setNumber(numberTxt);
        } else {
          setNumber(number + numberTxt);
        }
      } else {
        setNumber(number + numberTxt);
      }
    }
  };

  const deleteTxt = (numberTxt: string) => {
    let negative = '';
    let numberTemp = number;
    if (number.includes('-')) {
      negative = '-';
      numberTemp = number.substr(1);
    }

    if (numberTemp.length > 1) {
      setNumber(negative + numberTemp.slice(0, -1));
    } else {
      setNumber('0');
    }
  };

  const settBeforeNumber = () => {
    if (number.endsWith('.')) {
      setBeforeNumber(number.slice(0, -1));
    } else {
      setBeforeNumber(number);
    }

    setNumber('0');
  };
  const operationsEspecials = (numberTxt: string) => {
    switch (numberTxt) {
      case '+/-':
        if (number.includes('-')) {
          setNumber(number.replace('-', ''));
        } else {
          setNumber('-' + number);
        }
        break;
      case 'del':
        deleteTxt(numberTxt);
        break;

      case '+':
        settBeforeNumber()
        calcRef.current = Operators.addition;
        break;

      case 'x':
        settBeforeNumber()
        calcRef.current = Operators.multiplication;
        break;
      case '-':
        settBeforeNumber()
        calcRef.current = Operators.subtraction;
        break;

      case '÷':
        settBeforeNumber()
        calcRef.current = Operators.division;
        break;
      case '=':
        calcResult()
      default:
        return '0';
        break;
    }
  };

  const calcResult =()=>{
    if ( BeforeNumber === '0' ) return ;
    switch (calcRef.current) {
      case Operators.addition:
       
        setNumber(`${Number(BeforeNumber)+Number(number)}`)
        break;

        case Operators.subtraction:
          setNumber(`${Number(BeforeNumber)-Number(number)}`)
        break;

        case Operators.multiplication:
          setNumber(`${Number(BeforeNumber)*Number(number)}`)
        break;

        case Operators.addition:
          setNumber(`${Number(number)/Number(BeforeNumber)}`)
        break;
    
      default:
        break;
    }
  }

  return{ 
      BeforeNumber,
      number,
      clearResult,
      makeNumber
  }
}

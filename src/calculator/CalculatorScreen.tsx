import React, {useRef, useState} from 'react';
import {StatusBarIOS, TouchableOpacity} from 'react-native';
import {View, Text, SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import {colors} from '../theme/colors';
import ButtonCalc from './componets/ButtonCalc';
import useCalculator from './hooks/useCalculator';


export default function CalculatorScreen() {
  const {BeforeNumber,number,clearResult,makeNumber} = useCalculator()
  const buttons = [
    {
      id: 0,
      name: 'C',
      color: '#9B9B9B',
    },
    {
      id: 1,
      name: '+/-',
      color: '#9B9B9B',
    },
    {
      id: 2,
      name: 'del',
      color: '#9B9B9B',
    },
    {
      id: 3,
      name: '÷',
      color: '#FF9427',
    },
    {
      id: 4,
      name: '7',
    },

    {
      id: 5,
      name: '8',
    },

    {
      id: 6,
      name: '9',
    },

    {id: 7, name: 'x', color: '#FF9427'},
    {
      id: 8,
      name: '4',
    },
    {
      id: 9,
      name: '5',
    },

    {
      id: 10,
      name: '6',
    },
    {id: 11, name: '-', color: '#FF9427'},

    {
      id: 12,
      name: '3',
    },
    {
      id: 13,
      name: '2',
    },
    {
      id: 14,
      name: '1',
    },

    {id: 15, name: '+', color: '#FF9427'},

    {
      id: 16,
      name: '0',
      width: true,
    },

    {
      id: 17,
      name: '.',
    },

    {id: 18, name: '=', color: '#FF9427'},
  ];
  
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="black" barStyle="light-content" />
      <Text style={styles.resultSmall}>
        {BeforeNumber === '0' ? '' : BeforeNumber}
      </Text>
      <Text style={styles.result} numberOfLines={1} adjustsFontSizeToFit>
        {number}
      </Text>

      <View style={styles.containerButtons}>
        {buttons.map(item => {
          return (
            <ButtonCalc
              onpress={numberTxt =>
                item.name === 'C' ? clearResult() : makeNumber(numberTxt)
              }
              key={item.id}
              text={item.name}
              color={item.color}
              width={item.width}
            />
          );
        })}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
    paddingHorizontal: 10,
    justifyContent: 'flex-end',
  },

  containerButtons: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  result: {
    color: 'white',
    fontSize: 60,
    textAlign: 'right',
  },

  resultSmall: {
    color: 'rgba(225,225,225,0.7)',
    fontSize: 30,
    textAlign: 'right',
  },
});

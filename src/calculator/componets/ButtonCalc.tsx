import React from 'react';
import { Platform, Pressable } from 'react-native';
import {View, Text, TouchableOpacity, StyleSheet,} from 'react-native';

interface Props {
  color?: string;
  text: string;
  width?:boolean;
  onpress:(numberTxt:string)=>void;
}
export default function ButtonCalc({text, color="#333333",width,onpress}: Props) {
  const tamano = Platform.OS==="ios"? 168: 160
  return (
    <Pressable
        onPress={() => onpress(text)}
        style={({ pressed }) => [
          styles.button,
          {
            backgroundColor:color,
            borderRadius: pressed? 20:100,
            width: (width)?tamano: 75
          }
        ]}>
      <Text style={styles.buttonText}>{text}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    width: 75,
    height: 75,
    backgroundColor: '#333333',
    borderRadius: 100,
    justifyContent: 'center',
    margin: (Platform.OS==="ios")? 8:4,
  },

  buttonText: {
    color: 'white',
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'center',
    padding: 10,
  },
});

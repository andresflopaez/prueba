
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  useColorScheme,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CalculatorScreen from './src/calculator/CalculatorScreen';
import Home from './src/Home';
import ContextScreen from './src/contextApp/ContextScreen';


const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    flex: 1,
  };

  const Stack = createNativeStackNavigator();

  return (
    <SafeAreaView style={backgroundStyle}>
     <NavigationContainer>
     <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen options={{headerShown:false}} name="Calculator" component={CalculatorScreen} />
        <Stack.Screen  name="ContextApp" options={{title:"Context"}} component={ContextScreen} />

      </Stack.Navigator>
     </NavigationContainer>
    </SafeAreaView>
  );
};




export default App;
